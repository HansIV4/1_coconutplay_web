import Vue from 'vue';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import store from './store';

Vue.config.productionTip = false;

Vue.use(VueAxios, axios)
Vue.use(require('vue-moment'));

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
