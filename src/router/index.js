import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/pages/home/Index.vue';
import News from '@/pages/news/Index.vue';
import WriteReview from '@/pages/writeReview/index.vue';
import Login from '@/pages/login/index.vue'
import Register from '@/pages/register/index.vue'
import Profile from '@/pages/profile/index.vue'
import AdminPanel from '@/pages/adminPanel/index.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/categorie/:categorie',
    name: 'categorie',
    component: Home
  },
  {
    path: '/news/:id',
    name: 'news',
    component: News,
  },
  {
    path: '/writeReview',
    name: 'writeReview',
    component: WriteReview
  },
  {
    path: '/adminPanel',
    name: 'adminPanel',
    component: AdminPanel
  },
  {
    path: '/privateTests',
    name: 'privateTests',
    component: Home
  }
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;
