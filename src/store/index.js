import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from 'vuex-persistedstate'

//import item from "@/store/modules/monModule";
import JavaWebToken from "@/store/modules/JavaWebToken";

Vue.use(Vuex);

export default new Vuex.Store({
  plugins:[createPersistedState()],
  modules: {
    JavaWebToken,
  },
});
