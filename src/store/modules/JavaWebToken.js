const state = {
  JWT: ""
};

const mutations = {
  setJWT(state, JWT) {
    state.JWT = JWT;

  },
};

const actions = {

  getToken() {
    return state.JWT;
  },


  getJWT(context, JWT) {
    context.commit("setJWT", JWT);

  }
};



export default {
  namespaced: true,
  state,
  actions,
  mutations
};
